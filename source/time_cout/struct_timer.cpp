#include "../../headers/time_cout.h"

int main(int argc, char *argv[]) {

  lable("struct_timer");
  sublable("functions");
  // ---------------------------------------------------------------
  std::cout << "test cout_funtion via struct_timer\n";
  {
    Timer t;
    for (int i = 0; i < count_of_loop; i++)
      test();
  }

  // ---------------------------------------------------------------
  newline;
  std::cout << "test cout_inline_funtion via struct_timer\n";
  {
    Timer t;
    for (int i = 0; i < count_of_loop; i++)
      test_inline();
  }

  // ---------------------------------------------------------------
  sublable("direct cout");
  std::cout << "test cout via struct_timer\n";
  {
    Timer t;
    for (int i = 0; i < count_of_loop; i++)
      std::cout << char_print;
  }

  newline;
  return 0;
}
