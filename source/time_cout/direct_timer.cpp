#include "../../headers/time_cout.h"

int main(int argc, char *argv[]) {

  lable("direct_timer");
  sublable("functions");
  // takes start time
  std::cout << "test cout_funtion via direct timer\n";
  {
    auto start = Time::now();

    for (int i = 0; i < count_of_loop; i++)
      test();

    // take end time + count
    std::chrono::duration<float> a = Time::now() - start;

    std::cout << "takes " << a.count() * 1000.0f << "ms\n";
  }

  // ---------------------------------------------------------------
  newline;
  // takes start time
  std::cout << "test cout_inline_funtion via direct timer\n";
  {
    auto start2 = Time::now();

    for (int i = 0; i < count_of_loop; i++) {
      test_inline();
    }
    // take end time + count
    std::chrono::duration<float> a2 = Time::now() - start2;

    std::cout << "takes " << a2.count() * 1000.0f << "ms\n";
  }

  // ---------------------------------------------------------------
  newline;
  // takes start time
  std::cout << "test cout_inline_funtion via direct timer + old vars\n";
  {
    auto start = Time::now();
    std::chrono::duration<float> a;

    for (int i = 0; i < count_of_loop; i++)
      test_inline();

    // take end time + count
    a = Time::now() - start;

    std::cout << "takes " << a.count() * 1000.0f << "ms\n";
  }

  // ---------------------------------------------------------------
  sublable("direct cout");
  // takes start time
  std::cout << "test cout via direct timer\n";
  {
    auto start = Time::now();

    for (int i = 0; i < count_of_loop; i++)
      std::cout << char_print;

    // take end time + count
    std::chrono::duration<float> a = Time::now() - start;

    std::cout << "takes " << a.count() * 1000.0f << "ms\n";
  }

  // ---------------------------------------------------------------
  newline;
  // takes start time
  std::cout << "test cout via direct timer + old vars\n";

  {

    auto start = Time::now();
    std::chrono::duration<float> a;

    for (int i = 0; i < count_of_loop; i++)
      std::cout << char_print;

    // take end time + count
    a = Time::now() - start;

    std::cout << "takes " << a.count() * 1000.0f << "ms\n";
  }

  // ---------------------------------------------------------------
  newline;
  return 0;
}
